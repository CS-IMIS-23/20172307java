import junit.framework.TestCase;

import haitest.Complex;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a=new Complex(0,3);
    Complex b=new Complex(-1,-1);
    Complex c=new Complex(-1,-1);

    @Test
    public void testComplexAdd()throws Exception{
        assertEquals("-1.0+2.0i",a.ComplexAdd(b).toString());
    }

    @Test
    public void testComplexSub()throws Exception{
        assertEquals("1.0+4.0i",a.ComplexSub(b).toString());
    }

    @Test
    public void testComplexMulti()throws Exception{
        assertEquals("3.0-3.0i",a.ComplexMulti(b).toString());
    }

    @Test
    public void testComplexDiv()throws Exception{
        assertEquals("-1.5-1.5i",a.ComplexDiv(b).toString());
    }
    @Test
    public void testequals()throws Exception{
        assertEquals(true,b.equals(c));
    }

}