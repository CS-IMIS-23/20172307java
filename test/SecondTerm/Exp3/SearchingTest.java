package SecondTerm.Exp3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SearchingTest {
    Integer[] a ={2,0,1,7};
    Integer[] b ={2,3,0,7};
    Integer[] c ={2,0};
    Integer[] d ={1,7};
    Integer[] e ={2,3};
    Integer[] f ={0,7};
    Integer[] g ={20,17};
    Integer[] h ={23,7};
    Integer[] i ={2,0,1,7,2,3,0,7};
    Integer[] j ={20,17,23,7};

    @Test
    public void testlinearSearch() {
        assertEquals(true,Searching.linearSearch(a,0,a.length-1,1) );//正常情况
    }
    @Test
    public void testlinearSearch1() {
        assertEquals(false,Searching.linearSearch(b,0,b.length-1,20) );//异常情况
    }
    @Test
    public void testlinearSearch2() {
        assertEquals(true,Searching.linearSearch(c,3,c.length-1,2) );//边界
    }
    @Test
    public void testlinearSearch3() {
        assertEquals(true,Searching.linearSearch(d,0,c.length+1,2) );//边界
    }
}