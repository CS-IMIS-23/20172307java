package SecondTerm.Exp3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SortingTest {
    Integer[] a ={2,0,1,7};
    Integer[] b ={2,3,0,7};
    Integer[] c ={2,0};
    Integer[] d ={1,7};
    Integer[] e ={2,3};
    Integer[] f ={0,7};
    Integer[] g ={20,17};
    Integer[] h ={23,7};
    Integer[] i ={2,0,1,7,2,3,0,7};
    Integer[] j ={20,17,23,7};
    @Test
    public void testselectionSort() {
        assertEquals("0,1,2,7",Sorting.selectionSort(a));//正序情况
    }
    @Test
    public void testselectionSort1() {
        assertEquals("7,3,2,0",Sorting.selectionSort1(b));//逆序情况
    }


}