package test.homework;

import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(5.0,4.0);
    Complex b = new Complex(3.0,-1.0);
    Complex c = new Complex(5.0,4.0);

    @Test

    public void testComplexAdd()
    {
        assertEquals("8.0+3.0i",a.ComplexAdd(b).toString());
    }
    @Test
    public void testComplexSub()
    {
        assertEquals("2.0+5.0i",a.ComplexSub(b).toString());
    }
    @Test
    public void testComplexMulti()
    {
        assertEquals("19.0+7.0i",a.ComplexMulti(b).toString());
    }
    @Test
    public void testComplexDiv()
    {
        assertEquals("5.666666666666667-6.333333333333333i",a.ComplexDiv(b).toString());
    }
    @Test
    public void testequals()
    {
        assertEquals(true,a.equals(c));
    }

}