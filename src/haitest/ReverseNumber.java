package haitest;
//****************************************************************************************
//  ReverseNumber.java              Author:hyt
//  重构结队伙伴的类
//****************************************************************************************
import java.util.Scanner;
public class ReverseNumber
{
    private int reverse = 0;

    public static void main(String[] args)
    {
        int number,lastDigit;


        Scanner scan=new Scanner(System.in);

        System.out.print("Enter a positive integer: ");
        number=scan.nextInt();
        ReverseNumber a = new ReverseNumber();
        a.setReverse(number);

        System.out.println("That number reversed is " + a.getReverse());
    }
//重构
    public int getReverse() {
        return reverse;
    }

    public void setReverse(int number) {
        do
        {
            int lastDigit=number%10;
            reverse=(reverse*10)+lastDigit;
            number=number/10;
        }
        while(number>0);
    }
    @Override
    public String toString() {
        return "ReverseNumber{" +
                "reverse=" + reverse +
                '}';
    }
}