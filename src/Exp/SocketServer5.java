package Exp;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer5 {
    public static void main(String[] args) throws Exception {

        //建立联系，进行服务器和客户端之间的联系
        ServerSocket serversocket = new ServerSocket(8900);
        Socket socket = serversocket.accept();
        //用于向客户端发送数据的输出流对象
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        //用于接收客户端发来的数据的输入流对象
        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
        //通过KeyAgreee.getPassWord方法产生DHpbk.dat和DHprk.dat两个密钥文件中读取密文
        KeyAgree2 keyAgreee = new KeyAgree2();
        DigestPass digestPass = new DigestPass();
        String password = keyAgreee.getPassWord("DHpbk.dat", "DHprk.dat");
        byte[] byteContent = (byte[]) objectInputStream.readObject();
        String receiveDataStr = new String(AES.decrypt(byteContent, password));
        String md5 = digestPass.getMD5(receiveDataStr);
        String receiveMD5 = (String) objectInputStream.readObject();
        System.out.println("接收的密文：" + password);
        //输出两个MD5，判断两个MD5是否相等
        System.out.println("文件中读取的MD5值：" + receiveMD5);
        System.out.println("接收的MD5值：" + md5);
        if (receiveMD5.equals(md5)){
            System.out.println("MD5相等\n解密后的后缀表达式：" + receiveDataStr);
            Calculate calculate = new Calculate(receiveDataStr);
            calculate.ToResult();
            String result = calculate.getResult();
            System.out.println("计算结果：" + result);
            objectOutputStream.writeObject(result);
        }
        else{
            System.out.println("MD5不相等");
        }
        objectOutputStream.close();
        objectInputStream.close();
    }

}
