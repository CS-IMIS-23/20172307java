package Exp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketServer {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8900);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream,"UTF-8"));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info=null;
        String str = "";
        while(!((info = bufferedReader.readLine()) ==null)){
            System.out.println("我是服务器，用户需要排序的内容为 ：" + info);
            str = info;
        }
       String []str1=str.split("\\s");
        int[] list=new int[str1.length];
        for (int a=0;a<str1.length;a++ )
        {
            int b=Integer.parseInt(str1[a]);
            list[a]=b;
        }
        selection(list);
        String string="";
        for(int a=0;a<list.length;a++)
        {
            string+=list[a]+" ";
        }
        //给客户一个响应
        String reply="你需要排顺序的内容为："+str+"\n"+"排序之后是： "+string;
        printWriter.write(reply);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
    static void selection(int[] list) {
        int min;
        int temp;
        for (int index = 0; index < list.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < list.length; scan++)
                if (list[scan] < list[min])
                    min = scan;
            temp = list[min];
            list[min] = list[index];
            list[index] = temp;
        }
    }
}
