package Exp;


import test.homework.KeyAgree;
import test.homework.Key_DH;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;

public class SocketClient4 {
    public static void main(String args[]) throws Exception {
        Scanner scanner = new Scanner(System.in);
        Socket socket = new Socket("127.0.0.1", 8800);
        //数据输入流
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        System.out.print("请输入中缀表达式：");
        String zhongzhui = scanner.nextLine();
        Transform transform = new Transform(zhongzhui);
        transform.transform();
        String houzhui = transform.getLast();

        //公钥与私钥
        Key_DH.creat("DH2pub.txt","DH2pri.txt");
        FileInputStream fileInputStream = new FileInputStream("DH2pub.txt");
        ObjectInputStream objectInputStream1 = new ObjectInputStream(fileInputStream);
        Key key1 = (Key) objectInputStream1.readObject();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream1 = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream1.writeObject(key1);
        byte[] kb = byteArrayOutputStream.toByteArray();
        dataOutputStream.writeUTF(kb.length + "");
        for (int i = 0; i < kb.length; i++) {
            dataOutputStream.writeUTF(kb[i] + "");
        }
        int num = Integer.parseInt(dataInputStream.readUTF());
        byte str[] = new byte[num];
        for (int i = 0;i<num;i++) {
            String temp = dataInputStream.readUTF();
            str[i] = Byte.parseByte(temp);
        }
        ObjectInputStream objectInputStream2 = new ObjectInputStream (new ByteArrayInputStream (str));
        Key key2 = (Key)objectInputStream2.readObject();;
        FileOutputStream fileOutputStream = new FileOutputStream("DH1.txt");
        ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(fileOutputStream);
        objectOutputStream2.writeObject(key2);
        KeyAgree.creat("DH1.txt","DH2pri.txt");
        FileInputStream fileInputStreamm = new FileInputStream("miyao.txt");
        byte[] keysb = new byte[24];
        fileInputStreamm.read(keysb);
        System.out.print("最终密钥：");
        for (int n = 0;n<24;n++) {
            System.out.print(keysb[n]+" ");
        }
        System.out.println();
        SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
        Cipher cp = Cipher.getInstance("DESede");
        cp.init(Cipher.ENCRYPT_MODE, k);
        byte ptext[] = houzhui.getBytes("UTF8");
        //密文
        byte ctext[] = cp.doFinal(ptext);
        System.out.print("加密的后缀表达式：");
        for (int a = 0; a < ctext.length; a++) {
            System.out.print(ctext[a] + " ");
        }
        System.out.println();
        dataOutputStream.writeUTF(ctext.length + "");
        for (int b = 0; b < ctext.length; b++) {
            dataOutputStream.writeUTF(ctext[b] + "");
        }
        String answer = dataInputStream.readUTF();
        System.out.println("服务器的答案：" + answer);
    }
}
