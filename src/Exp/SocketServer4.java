package Exp;


import test.homework.KeyAgree;
import test.homework.Key_DH;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;

public class SocketServer4 {
    public static void main(String[] args) {
        try {

            ServerSocket serversocket = new ServerSocket(8800);
            Socket socketOnServer = serversocket.accept();
            DataOutputStream dataOutputStream = new DataOutputStream(socketOnServer.getOutputStream());
            DataInputStream dataInputStream = new DataInputStream(socketOnServer.getInputStream());
            //公钥与私钥
            Key_DH.creat("DH1.txt", "DH1pri.txt");
            int num = Integer.parseInt(dataInputStream.readUTF());
            byte np[] = new byte[num];
            for (int i = 0;i<num;i++) {
                String temp = dataInputStream.readUTF();
                np[i] = Byte.parseByte(temp);
            }
            ObjectInputStream objectInputStream = new ObjectInputStream (new ByteArrayInputStream (np));
            Key key1 = (Key)objectInputStream.readObject();;
            FileOutputStream fileOutputStream1 = new FileOutputStream("Cpub.txt");
            ObjectOutputStream objectOutputStream1 = new ObjectOutputStream(fileOutputStream1);
            objectOutputStream1.writeObject(key1);
            FileInputStream fileInputStream = new FileInputStream("DH1.txt");
            ObjectInputStream bp = new ObjectInputStream(fileInputStream);
            Key key2 = (Key) bp.readObject();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream2.writeObject(key2);
            byte[] str = byteArrayOutputStream.toByteArray();
            dataOutputStream.writeUTF(str.length + "");
            for (int i = 0; i < str.length; i++) {
                dataOutputStream.writeUTF(str[i] + "");
            }
            KeyAgree.creat("Cpub.txt", "DH1pri.txt");
            String string = dataInputStream.readUTF();
            byte ctext[] = new byte[Integer.parseInt(string)];
            for (int i = 0;i<Integer.parseInt(string);i++) {
                String temp = dataInputStream.readUTF();
                ctext[i] = Byte.parseByte(temp);
            }
            // 获取密钥
            FileInputStream f = new FileInputStream("miyao.txt");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.print("传输的密钥（byte型）：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+" ");
            }
            System.out.println();
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            // 密文
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);

            for (int i = 0; i < ptext.length; i++) {
                System.out.print(ptext[i] + " ");
            }
            System.out.println();
            // 明文
            String p = new String(ptext, "UTF8");
            System.out.println("服务器解密后的后缀表达式：" + p);
           Calculate calculate = new Calculate(p);
           calculate.ToResult();
            dataOutputStream.writeUTF(calculate.getResult());
            System.out.println("服务器计算结果：" + calculate.getResult());
        } catch (Exception e) {
            e.getStackTrace();
            e.getMessage();
        }
    }
}
