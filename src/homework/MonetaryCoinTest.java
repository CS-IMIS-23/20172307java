package homework;
//*********************************************************************************************************************
//    MonetaryCoinTest.java                     Author:hyt
//     测试抛硬币
//*********************************************************************************************************************
public class MonetaryCoinTest {
    public static void main(String[] args) {
        MonetaryCoin a = new MonetaryCoin(1.0);
        MonetaryCoin b = new MonetaryCoin(2.0);

        //计算俩个对象的和
        double sum = a.getFacevalue()+ b.facevalue;
        System.out.println(sum);

        //演示继承
        a.flip();
        System.out.println(a);

    }
}
