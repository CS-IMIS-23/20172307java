package homework;
//*********************************************************************************************************************
//   account2.java                      Author:hyt
//   PP8.6为了实现user的功能,保存客户信息。
//*********************************************************************************************************************

public class account2 {
    private account1[] acc;
    private int n;

    public account2() {
        acc = new account1[30];
        n = 0;
    }

    public void addAccount(String owner, long account, double initial) {
        if (n < acc.length) {
            acc[n] = new account1(owner, account, initial);
            n++;
        }
    }

    public void getDrawMoney(String owner, long account, double initial) {
        for (int u = 0; u < 30; u++)
            if (acc[u] != null)
                if (acc[u].getName().equals(owner) && acc[u].getNumber() == account)
                    acc[u].deposit(initial);
    }

    public void getDraw(String owner, long account, double initial) {
        for (int n = 0; n < 30; n++)
            if (acc[n] != null)
                if (acc[n].getName().equals(owner) && acc[n].getNumber() == account)
                    if (acc[n].getNumber() > initial)
                        acc[n].withdraw(initial);
    }

    public void addaccrual() {
        int b = 0;
        while (b < acc.length) {
            if (acc[b] != null) {
                acc[b].addInterest();
            }
            b++;
        }
    }

    public String toString() {
        String Result = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        for (int d = 0; d < 30; d++) {
            if (acc[d] != null)
                Result += acc[d].toString() + "\n\n";
        }
        return Result;
    }
}
