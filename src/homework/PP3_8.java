//*********************************************************************
//   PP3_8.java			Author:hyt
//              wekk3homework
//*********************************************************************

import java.util.Random;

public class PP3_8
{
   public static void main(String[] args)
   {
       Random ss = new Random();
       int a;
       double b, c, d;
       a = ss.nextInt(21) + 20;
       b = Math.sin(a);
       c = Math.cos(a);
       d = Math.tan(a);
       System.out.println("Sin: " + b);
       System.out.println("Cos: " + c);
       System.out.println("Tan: " + d);
    }
}
