//*****************************************************************************
//   Car.java			Author:hyt
//              week4homework
//*****************************************************************************

public class Car
{
  private String name;
  private String type;
  private int time;
  private final int line = 1973;

  public Car(String nm, String tp, int tm)
  {
     name = nm;
     type = tp;
     time = tm;
  }
  
  public void setName(String nm1)
  {
    name = nm1;
  }
  public String getName()
  {
    return name;
  }
  
  public void setType(String te1)
  {
    type = te1;
  }
  public String getType()
  {
    return type;
  }

  public void setTime(int ti1)
  {
    time = ti1;
  }
  public int Time()
  {
    return time;
  }
  
  public String toString()
  {
     return name + "\t" + type + "\t" + time;
  }

  public Boolean isAntique()
  {
     return (time < line);
  }

}
   

