package homework;

import homework.PP9_3.Magazine;

//**************************************************************************************
//   DVDList.java           Author:hyt
//   创建DVD集合
//**************************************************************************************
public class DVDList {
    private DVDNode list;

    public DVDList()
    {
        list = null;
    }
    public void add(DVD1 dvd1) {
        DVDNode node = new DVDNode(dvd1);
        DVDNode current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }
    public String toString() {
        String result = "";
        DVDNode current = list;
        while (current != null)
        {
            result += current.dvd + "\n";
            current = current.next;
        }
        return result;
    }
    public class DVDNode
    {
        public DVD1 dvd;
        public DVDNode next;

        public DVDNode(DVD1 dvd1)
        {
            dvd = dvd1;
            next = null;
        }
    }

}
