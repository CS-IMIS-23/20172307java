package homework;

import java.util.Scanner;

//*****************************************************************************************************************
//  StringTooLongExceptionTest.java                 Author:hyt
//  测试StringTooLOngException类
//*****************************************************************************************************************
public class StringTooLongExceptionTest {
    public static void main(String[] args) throws StringTooLongException {
        final int max = 20;
        String str = "";
        String str1 = "";
        StringTooLongException problem = new StringTooLongException("String is too long.");
        Scanner scanner = new Scanner(System.in);

        while (!str.equals("Done"))
        {
            System.out.println("请输入一个字符： ");
            str1 += scanner.nextLine();
            System.out.println("结束输入请输入Done: ");
            str = scanner.nextLine();

        }
        if (str1.length()>20)
            throw problem;
        else
            System.out.println(str1);
    }
}
