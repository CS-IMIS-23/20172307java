package homework;

import java.util.Scanner;

//**********************************************************************************
//  PalindromeTester2.java              Author:hyt
//  PalindromeTester的递归版本
//**********************************************************************************
public class PalindromeTester2 {
    //--------------------------------------------------------------------------
    //  Tests strings to see if they are pailindromes.
    //--------------------------------------------------------------------------
    public static void main(String[] args) {
        String str, another = "y";
        int left, right;

        Scanner scan = new Scanner(System.in);

        while (another.equalsIgnoreCase("y"))// allows y or Y
        {
            System.out.println("Enter a potential palindrome:");
            str = scan.nextLine();

            left = 0;
            right = str.length() - 1;
            pal(left, right, str);


            System.out.println();
            if (pal(left, right, str)==false)

                System.out.println("That string is NOT a palindrome.");
            else
                System.out.println("That string Is a palindrome.");

            System.out.println();
            System.out.print("Test another palindrome.");
            another = scan.nextLine();
        }
    }

    static boolean pal(int left, int Right, String str) {
        boolean result = true;

        if (str.charAt(left) == str.charAt(Right) && left < Right) {
            left++;
            Right--;
            pal(left, Right, str);
        }else
             result = false;

        return result;

    }
}





