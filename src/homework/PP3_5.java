//*********************************************************************
//   PP3_5.java			Author:hyt
//               week3homework
//*********************************************************************
import java.util.Scanner;

public class PP3_5
{
   public static void main(String[] args)
   {
     int x1, y1;
     int x2, y2, a, b;
     double d, c;
     
     Scanner scan = new Scanner(System.in);
     
     System.out.print("Enter the x1 : ");
     x1 = scan.nextInt();

     System.out.print("Enter the y1 : ");
     y1 = scan.nextInt();

     System.out.print("Enter the x2 : ");
     x2 = scan.nextInt();

     System.out.print("Enter the y2 : ");
     y2 = scan.nextInt();

     a = x1-x2;
     b = y1-y2;
     c = Math.pow(a,2) - Math.pow(b,2);
     d = Math.sqrt(c);
     System.out.println("Distance : "+ d);
     }
}
