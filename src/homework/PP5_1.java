//***************************************************************************
//  PP5_1.java			Author:hyt
//             week5homework
//***************************************************************************
import java.util.Scanner;
public class PP5_1
{
   public static void main(String[] args)
   {
      int year;
      Scanner scan = new Scanner(System.in);

      System.out.println("Enter a year : ");
      year = scan.nextInt();
      Integer n = new Integer(year % 4);
      Integer m = new Integer(year % 400);
      if (year >= 1582)
         if (n == 0 ||( m == 0))
            System.out.println("It's a leap year");
         else
            System.out.println("It's not a leap year");
      else
         System.out.println("You entered a wrong year.");
   }
}
