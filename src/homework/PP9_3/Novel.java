package homework.PP9_3;
//********************************************************************************************************************
//  Novel.java                                  Author:hyt
//  创建Novel类继承ReadingMaterial类
//********************************************************************************************************************
public class Novel extends  ReadingMaterial{
    protected String kind;
    public Novel(double page, String keywords, String name,String kind) {
        super(page, keywords, name);
        this.kind = kind;
    }

    public String getKind() {
        return kind;
    }
}
