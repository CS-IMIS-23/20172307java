package homework.PP9_3;
//********************************************************************************************************************
//  Textbook.java                   Author:hyt
//  创建Textbook类继承ReadingMaterial类
//********************************************************************************************************************
public class TextBook extends ReadingMaterial {

    public TextBook(double page, String keywords,String name) {
        super(page, keywords,name);
    }
}
