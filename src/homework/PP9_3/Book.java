package homework.PP9_3;
//********************************************************************************************************************
//   Book.java                          Author:hyt
//   创建Book类，继承ReadingMaterial类
//********************************************************************************************************************
public class Book extends ReadingMaterial {
    public Book(double page, String keywords, String name) {
        super(page, keywords, name);
    }
}
