package homework.PP9_3;
//******************************************************************************************************************
//  test.java                               Author:hyt
//  测试
//******************************************************************************************************************
public class test {
    public static void main(String[] args) {
        Book a = new Book(50,"缘分","缘分");
        Novel b = new Novel(100,"无缘","无缘","解忧");
        Magazine c = new Magazine(200,"有缘","No.15","有缘再会");
        TextBook d = new TextBook(300,"缘分介绍","缘分介绍书");

        System.out.println("书名：" + a.getName()+"\t" +"页数："+ a.getPage()+"\t"+"关键字： "+ a.getKeywords());
        System.out.println("书名：" + b.getName()+"\t" +"页数："+ b.getPage()+"\t"+"关键字： "+ b.getKeywords()+ "\t"+"类型： " + b.getKind());
        System.out.println("书名：" + c.getName()+"\t" +"页数："+ c.getPage()+"\t"+"关键字： "+ c.getKeywords()+"\t"+"期数: "+c.getIssue());
        System.out.println("书名：" + a.getName()+"\t" +"页数："+ a.getPage()+"\t"+"关键字： "+ a.getKeywords());

    }
}
