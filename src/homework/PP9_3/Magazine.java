package homework.PP9_3;
//*********************************************************************************************************************
//  Magazine.java                       Author:hyt
//  创建Magazine类，继承ReadingMaterial类
//*********************************************************************************************************************
public class Magazine extends ReadingMaterial {
    private  String Issue;
    public Magazine(double page, String keywords,String Issue,String name) {
        super(page, keywords,name);
        this.Issue = Issue;
    }

    public String getIssue() {
        return Issue;
    }
}
