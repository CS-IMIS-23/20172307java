package homework;
//********************************************************************************************************************
//     PP8_1.java                   Author:hyt
//                week6homework
//********************************************************************************************************************
import java.util.Scanner;
public class PP8_1 {
    public static void main(String[] args) {
        int[] times = new int[51];
        Scanner scan = new Scanner(System.in);
        int num = 0;
        while (num >= 0 && num<50 )
        {
            System.out.println("Enter an number between 0 to 50 ");
            num = scan.nextInt();
            times[num]++;
            System.out.println("If you want to quit,Enter an number out of this range: ");
            num = scan.nextInt();
        }

        System.out.println();
        for (int n = 0; n < times.length; n++)
        {
            System.out.print(n);
            System.out.println(": " + times[n]);
        }
    }

}
