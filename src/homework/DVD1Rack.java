package homework;
//***********************************************************************************
//  DVD1.java               Author:hyt
//  测试DVDList类
//************************************************************************************
public class DVD1Rack {
    public static void main(String[] args) {
        DVDList rack = new DVDList();

        rack.add(new DVD1("The Godfather","Francis Ford Coppola",1972,24.95,true));
        rack.add(new DVD1("District 9 ", "Neill Blokamp",2009,19.95,false));
        System.out.println(rack);
    }
}
