//*****************************************************************************
//   PairOfDice.java			Author:hyt
//                    week4homework	
//*****************************************************************************
public class PairOfDice
{
   private final int MAX = 6; 
   private int facevalue1, facevalue2;

   public PairOfDice()
   {
     facevalue1 = 1;
     facevalue2 = 1;
   }
   

   public int roll()
   {
      facevalue1 = (int)(Math.random()* MAX) + 1;
      facevalue2 = (int)(Math.random()* MAX) + 1;

      return facevalue1 + facevalue2;
      
   }
    
   public void setFacevalue1(int value1)
   {
      facevalue1 = value1;
   }
   public int getFacevalue1()
   {
      return facevalue1;
   }
   
   
   public void setFacevalue2(int value2)
   {
      facevalue2 = value2;
   }
   public int getFacevalue2()
   {
      return facevalue2;
   }
   
   public String toString()
   {
      String result = Integer.toString(facevalue1 + facevalue2);
      return result;
   }
} 
