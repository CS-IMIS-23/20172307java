//**********************************************************************
//  PP7.1		Author:hyt
//        week4homework
//**********************************************************************
public class transactions
{
    public static void main(String[] args)
    {
       account acct1 = new account("Ted Murphy", 72354);
       account acct2 = new account("Jane Smith", 69713);
       account acct3 = new account("Edward Demsey", 93757);

       acct1.deposit(25.85);

       double smithBalance = acct2.deposit(500.00);
       System.out.println("Smith balance after deposit: " +
                          smithBalance);

       System.out.println("Smith balance after withdrawal: " +
                          acct2.withdraw (430.75, 1.50));

       acct1.addInterest();
       acct2.addInterest();
       acct3.addInterest();

       System.out.println();
       System.out.println(acct1);
       System.out.println(acct2);
       System.out.println(acct3);
     }
}
