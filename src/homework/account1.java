package homework;
//*********************************************************************************************************************
//   account1.java                          Author:hyt
//   完成PP8.6。
//*********************************************************************************************************************

import java.text.NumberFormat;

public class account1 {
    private final double RATE = 0.035; // interest rate of 3.5%

    private long acctNumber;
    private double balance;
    private String name;

    //--------------------------------------------------------------------------
    //  Sets up the account by defining its owner, account number,
    //  and initial balance.
    //--------------------------------------------------------------------------
    public account1(String owner, long account1, double initial) {

        name = owner;
        acctNumber = account1;
        balance = initial;
    }

    //--------------------------------------------------------------------------
    //  Get the owner's name.
    //--------------------------------------------------------------------------
    public String getName()
    {
        return name;
    }

    //--------------------------------------------------------------------------
    //  Get the owner's number.
    //--------------------------------------------------------------------------
    public long getNumber()
    {
        return acctNumber;
    }

    //--------------------------------------------------------------------------
    //  Deposits the specified amount into the account. Returns the
    //  new balance.
    //--------------------------------------------------------------------------
    public double deposit(double amount) {
        balance = balance + amount;
        return balance;
    }

    //--------------------------------------------------------------------------
    //  Withdraws the specified amount from the account and applies
    //  the fee. Returns the new balance.
    //--------------------------------------------------------------------------
    public double withdraw(double amount) {
        balance = balance - amount ;
        return balance;
    }

    //--------------------------------------------------------------------------
    //  Adds interest to the account and returns the new balance.
    //--------------------------------------------------------------------------
    public double addInterest() {
        balance += (balance * RATE);
        return balance;
    }

    //--------------------------------------------------------------------------
    // Returns a one-line description of the account as a string
    //--------------------------------------------------------------------------
    public double getBalance() {
        return balance;
    }

    //---------------------------------------------------------------------------   // Returns a one-line description of the account as a string.
    //---------------------------------------------------------------------------
    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        return acctNumber + "\t" + name + "\t" + fmt.format(balance);
    }
}