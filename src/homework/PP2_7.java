//**********************************************************************
//	PP2_7.java		   Author:hyt
//	           week2homework
//**********************************************************************

import java.util.Scanner;

public class PP2_7
{  
   public static void main(String[] args)
   {
      int v, s;
      double t;
      Scanner scan = new Scanner(System.in);

      System.out.print("Enter the number of speed:");
      v = scan.nextInt();
      System.out.print("Enter the number of distance:");
      s = scan.nextInt();

      t = v / s;
      System.out.println(t);
   }
}
      
