//*******************************************************************
//	PP2_12.java		Author:hyt
//		  week2homework
//*******************************************************************

import java.util.Scanner;

public class PP2_12
{
   public static void main(String[] args)
   
   {
     
      int x, c, s;
   
      Scanner scan = new Scanner(System.in);

      System.out.print("Enter the square side length.");
      x = scan.nextInt();

      c = x*4;
      s = x*x;
      System.out.println("perimeter" + c);
      System.out.println("area" + s);
   }
}
