package homework;

import eg.DVD;

import java.text.NumberFormat;

public class DVDCollection implements Comparable{
    private DVD[] collection;
    private int count;
    private double totalCost;
    private String title, director;
    private int year;
    private double cost;
    private boolean bluray;



    //----------------------------------------------------------------------------------------------------------------
    //  Constructor: Creates an initially empty collection.
    //----------------------------------------------------------------------------------------------------------------
    public DVDCollection(String title, String director, int year, double cost, boolean bluray)
    {
        collection = new DVD[100];
        count = 0;
        totalCost = 0.0;

        this.title = title;
        this.director = director;
        this.year = year;
        this.cost = cost;
        this.bluray = bluray;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Adds a DVD to the collection, increasing the size of the
    //collection array if necessary.
    //-----------------------------------------------------------------------------------------------------------------
    public void addDVD(String title, String director, int year,
                       double cost, boolean bluray)
    {
        if (count== collection.length)
            increaseSize();

        collection[count] = new DVD(title, director, year, cost, bluray);
        totalCost += cost;
        count++;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Returns a report decribing the DVD collection
    //-----------------------------------------------------------------------------------------------------------------


    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();

        String report = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        report += "My DVD Collection\n\n";

        report += "Total cost: " + fmt.format(totalCost) + "\n";
        report += "Average cost: " + fmt.format(totalCost/count);

        report += "\n\nDVD List:\n\n";

        for (int dvd = 0; dvd < count; dvd++)
            report += collection[dvd].toString() + "\n";

        return report;
    }

    //-----------------------------------------------------------------------------------------------------------------
    //  Increases the capacity of the collection by creating a
    //  Iarger array and copying the existing collection into it.
    //-----------------------------------------------------------------------------------------------------------------
    private void increaseSize()
    {
        DVD[] temp = new DVD[collection.length * 2];

        for (int dvd = 0; dvd < collection.length; dvd++)
            temp[dvd] =collection[dvd];

        collection = temp;
    }

    public int compareTo(Object o) {
        int result;

        String ti = ((DVDCollection) o).getTitle();
        result = title.compareTo(ti);
        return result;

    }

    public String getTitle() {
        return title;
    }
}

