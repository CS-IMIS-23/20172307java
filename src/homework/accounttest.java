package homework;
//********************************************************************************************************************
//  accounttest.java            Author:hyt
//  测试account
//********************************************************************************************************************
public class accounttest {
    public static void main(String[] args) {
        account2 account = new account2();

        account.addAccount("Huang",20172307,10000);
        account.addAccount("TX",20172305,20000);

        account.getDraw("Huang",20172307,60000);
        account.getDrawMoney("Huang",20172307,10000);

        System.out.println(account);

        account.addaccrual();
        System.out.print("After accrual: ");
        System.out.println(account);
    }
}
