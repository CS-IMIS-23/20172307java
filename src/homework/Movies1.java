package homework;

public class Movies1 {
    //-----------------------------------------------------------------------------------------------------------------
    //  Creates a DVDCollection object and adds some DVDs to it. Prints
    //  reports on  the status of the collection.
    //-----------------------------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        DVDCollection[] movies = new DVDCollection[7];

        movies[0] = new DVDCollection("The Godfather", "Francis Ford Coppola", 1972, 24.95, true);
        movies[1] = new DVDCollection("District 9", "Neill Blomkamp", 2009, 19.95, false);
        movies[2] = new DVDCollection("Iron Man","Jon Favreau", 2008, 15.95, false);
        movies[3] = new DVDCollection("All About Eve", "Joseph Mankiewicz", 1950, 17.50, false);
        movies[4] = new DVDCollection("The Matrix", "Andy & Lana Wachowski", 1999, 19.95, true);
        movies[5] = new DVDCollection("Iron Man 2","Jon Favreau", 2010, 22.99, false);
        movies[6] = new DVDCollection("Casablanca","Michael Curtiz", 1942, 19.95, false);

        Sorting1.selectionSort(movies);

        for (DVDCollection mov:movies)
            System.out.println(mov);
    }
}
