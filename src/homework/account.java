//*****************************************************************************
//    PP7.1			Author:hyt
//           week4homework
//*****************************************************************************

import java.text.NumberFormat;

public class account
{
   private final double RATE = 0.035; // interest rate of 3.5%

   private long acctNumber;
   private double balance;
   private String name;

   //--------------------------------------------------------------------------
   //  Sets up the account by defining its owner, account number,
   //  and initial balance.
   //--------------------------------------------------------------------------
   public account(String owner, long account1, double initial)
   {

       name = owner;
       acctNumber = account1;
       balance = initial;
   }
  
   public account(String owner, long account1)
   {
       name = owner;
       acctNumber = account1;
       
   }

   //--------------------------------------------------------------------------
   //  Deposits the specified amount into the account. Returns the
   //  new balance.
   //--------------------------------------------------------------------------
   public double deposit(double amount)
   {
       balance = balance + amount;
       return balance;
   }

   //--------------------------------------------------------------------------
   //  Withdraws the specified amount from the account and applies
   //  the fee. Returns the new balance.
   //--------------------------------------------------------------------------
   public double withdraw(double amount, double fee)
   {
       balance = balance - amount - fee;
       return balance;
   }
   //--------------------------------------------------------------------------
   //  Adds interest to the account and returns the new balance.
   //--------------------------------------------------------------------------
   public double addInterest()
   {
      balance += (balance * RATE);
      return balance;
   }

   //--------------------------------------------------------------------------
   // Returns a one-line description of the account as a string
   //--------------------------------------------------------------------------
   public double getBalance()
   {
      return balance;
   }

   //---------------------------------------------------------------------------   // Returns a one-line description of the account as a string.
   //---------------------------------------------------------------------------   
   public String toString()
   {
      NumberFormat fmt = NumberFormat.getCurrencyInstance();
      return acctNumber + "\t" + name + "\t" + fmt.format(balance);
   }
}

