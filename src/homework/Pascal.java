package homework;

import java.util.Scanner;

//****************************************************************************************************
//  Pascal.java                 Author:hyt
//  递归输出Pascal三角的第n行。
//****************************************************************************************************
public class Pascal {
    static int pas(int n,int m){
       if (m == 0 || n==m)
           return 1;
       else
           return pas(n-1,m) + pas(n-1,m-1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num;
        System.out.println("请输入Pascal的第几行： ");
        num = scanner.nextInt();

        int[][] cal = new int[num][num];
        for (int n = 0; n < num; n++) {
            for (int m = 0; m <= n; m++)
                cal[n][m] = pas(n, m);
        }

        System.out.println("Pascal行的第"+num+"行：");
        for (int m = 0; m< num; m++)
            System.out.print(cal[(num - 1)][m] + " ");
    }

}
