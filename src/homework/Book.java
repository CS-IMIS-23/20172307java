
public class Book {
    private String bookname;
    private String author;
    private String press;
    private String copyrightdate;
 public Book(String name,String Author,String Press,String Copyrightdate)
{
  bookname = name ;
  press = Press ;
  author = Author;
  copyrightdate = Copyrightdate;

    }
    public void setBookname(String name)
    {
        bookname = name;

    }

    public String getBookname() {
        return bookname;
    }
    public void setAuthor(String Author){
        author = Author;
    }

    public String getAuthor() {
        return author;
    }
    public void setPress(String Press){
        press = Press;
    }

    public String getPress() {
        return press;
    }

    public void setCopyrightdate(String Copyrightdate) {
        copyrightdate = Copyrightdate;
    }

    public String getCopyrightdate() {
        return copyrightdate;
    }

    public String toString() {
        String result = (bookname + author + press +copyrightdate);
        return result;
    }

}
