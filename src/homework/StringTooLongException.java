package homework;
//************************************************************************************************************
//  StringTooLongException.java                     Author:hyt
//  PP11.1抛出字符串过长的异常
//************************************************************************************************************
public class StringTooLongException extends Exception{
    StringTooLongException(String message)
    {
        super(message);
    }
}
