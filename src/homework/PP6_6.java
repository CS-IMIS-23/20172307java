package homework;
//*******************************************************************************************************************
//  PP6_6.java              Author:hyt
//
//  Demonstrates the use of an if-else statement.
//*******************************************************************************************************************

public class PP6_6 {
    public static void main(String[] args) {
        Coin myCoin = new Coin();
        int n;
        int m = 1;
        for (n = 1; n <= 100; n++) {
            myCoin.flip();
            if (myCoin.isHeads())
                m++;

        }
        System.out.println("Times of Heads :" + m);
    }
}
