package homework;

//*****************************************************************************
//   SelectionSort.java                 Author:hyt
//   以链表的形式编写一个新的选择法排序
//*****************************************************************************
public class SelectionSort {
    private NumberNode list;

    public SelectionSort() {
        list = null;
    }

    private class NumberNode {
        public int num;
        public NumberNode next;

        public NumberNode(int num1) {
            num = num1;
            next = null;
        }

    }

    public void add(int num) {
        NumberNode node = new NumberNode(num);
        NumberNode current;
        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public void sort() {
        NumberNode min;
        NumberNode temp;
        NumberNode numNode = list;

        while (numNode != null) {
            min = numNode;
            NumberNode current = min.next;
            while (current != null) {
                if (current.num < min.num) {
                    min = current;
                }
                current = current.next;
            }
            temp = min.next;
            min.next = numNode.next;
            numNode.next = temp;

            numNode = numNode.next;
        }
    }
}


