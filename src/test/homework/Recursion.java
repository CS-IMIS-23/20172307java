package test.homework;

import java.io.*;
import java.util.Scanner;

public class Recursion {
    static int f (int n)
    {
        if(n==0)
            return 0;
        else if (n==1)
            return 1;
        else
            return f(n-1)+f(n-2);
    }

    public static void main(String[] args) throws IOException {
        int n;
        Scanner scan = new Scanner(System.in);
        System.out.println("输入一个数: ");
        n = scan.nextInt();
        String result = f(n) + "";
        OutputStream outputStream = new FileOutputStream("D:\\test.txt");
        outputStream.write(result.getBytes());
        outputStream.close();

        }
    }



