package test.homework;

public class Cow extends Animal {
    public Cow(String name, int id) {
        super(name, id);
    }

    public void eat() {
        System.out.println("I'm eating");
    }

    @Override
    public void sleep() {
        System.out.println("I want to sleep");

    }

    @Override
    public void introduction() {
        System.out.println("My name is " + getName() + "My id is " + getId());

    }
}

