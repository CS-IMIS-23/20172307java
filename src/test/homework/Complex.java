package test.homework;
//********************************************************************************************************************
//   Complex.java               Author:hyt
//                实验2测试4
//********************************************************************************************************************

public class Complex {
    private double Realpart;
    private double Imagepart;;
    //构造函数
    public Complex(double R, double I)
    {
        Realpart = R;
        Imagepart = I;
    }
    //建立setter
    public void setImagepart(double imagepart) {
        Imagepart = imagepart;
    }
    public double getImagepart() {
        return Imagepart;
    }
    public void setRealpart(double realpart)
    {
        Realpart = realpart;
    }
    //建立getter
    public double getRealpart() {
        return Realpart;
    }
    //构造加法
    public Complex ComplexAdd(Complex a)
    {
        return new Complex(Realpart+a.getRealpart(),Imagepart+a.getImagepart());
    }
    //构造减法
    public Complex ComplexSub(Complex a)
    {
        return new Complex(Realpart-a.getRealpart(),Imagepart-a.getImagepart());
    }
    //构造乘法
    public Complex ComplexMulti(Complex a)
    {
        return new Complex(Realpart*a.getRealpart()-Imagepart*a.getImagepart(),Realpart*a.getImagepart()+Imagepart*a.getRealpart());
    }
    //构造除法
    public Complex ComplexDiv(Complex a)
    {
        return new Complex(Realpart/a.getRealpart()-Imagepart/a.getImagepart(),Realpart/a.getImagepart()-Imagepart/a.getRealpart());
    }
    //构造等于
    public boolean equals(Complex a)
    {
        if (Realpart==a.getRealpart()&&Imagepart==a.getImagepart())
            return true;
        else
            return false;
    }

    public String toString() {
        String n = " ";
        if (Imagepart > 0)
            n = Realpart + "+"+ Imagepart + "i";
        else if  (Imagepart == 0)
            n = Realpart + "";
        else
            n = Realpart + "" + Imagepart + "i";
        return n;
    }
}
