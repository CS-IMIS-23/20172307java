package test.homework;

public class MagazineList
{
    private MagazineNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public MagazineList()
    {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag)
    {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else
        {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString()
    {
        String result = "";

        MagazineNode current = list;

        while (current != null)
        {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }

    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //*****************************************************************
    private class MagazineNode
    {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag)
        {
            magazine = mag;
            next = null;
        }
    }
    //插入
    public void insert(int insert , Magazine newmagazine)
    {
       int n = 1;
       MagazineNode node = new MagazineNode(newmagazine);
       MagazineNode former;
       MagazineNode backer;
       if (insert == 1)
       {
           node.next = list;
           list = node;
       }
       else
       {
           former = list;
           while (n < insert-1)
           {   former = former.next;
           n++;
           }
           backer = former.next;
           node.next = backer;
           former.next = node;

       }
    }
    //删除
    public void delete(Magazine delNode)
    {
        MagazineNode former;
        MagazineNode current;

        former = list;
        current = former.next;
        if (delNode.toString().equals(former.magazine.toString()))
            list = former.next;
        else
        {
            while (!current.magazine.toString().equals(delNode.toString()))
                former = former.next;
            former.next = current.next;
        }

    }

    }



