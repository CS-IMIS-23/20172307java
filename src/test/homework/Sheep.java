package test.homework;

public class Sheep extends Animal {
    public Sheep(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("I'm eating");
    }

    @Override
    public void sleep() {
        System.out.println("I want to sleep");

    }

    @Override
    public void introduction() {
        System.out.println("My name is "+ getName() + "My id is " + getId());

    }
}



