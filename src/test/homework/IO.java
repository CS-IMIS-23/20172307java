package test.homework;

import java.io.*;
import java.util.Scanner;

public class IO {
    //排序
    static void selection(int[] list) {
        int min;
        int temp;
        for (int index = 0; index < list.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < list.length; scan++)
                if (list[scan] < list[min])
                    min = scan;
            temp = list[min];
            list[min] = list[index];
            list[index] = temp;
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str;
        String str0="" ;
        String str2 = "";
        System.out.println("请输入一列数： ");
        str = scan.nextLine();
        //输入文件
        try {
            FileOutputStream fileOutputStream= new FileOutputStream("D:\\test.txt");
            fileOutputStream.write(str.getBytes());
            fileOutputStream.close();
        } catch (IOException exception) {
            System.out.println("IOException");
        }
        //输出文件
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\test.txt")));
            char[] chars = new char[10];
            int length = reader.read(chars);
            for (int d = 0; d < chars.length; d++) {
                str0 += chars[d];
            }
            reader.close();
        }
        catch (IOException exception)
        {
            System.out.println("IOException");
        }
        //实现排序
        String[] str1 = str.split("\\s");
        int[] num = new int[str1.length];
        for (int n = 0; n < str1.length; n++) {
            num[n] = Integer.parseInt(str1[n]);
        }
        selection(num);
        for (int x = 0; x < num.length; x++) {
            str2 = str2 + num[x] + " ";
        }
        //重新输入文件
        try {
            OutputStream outputStream12 = new FileOutputStream("D:\\test.txt", true);
            outputStream12.write("\r\n".getBytes());
            outputStream12.write(str2.getBytes());
        }
        catch (IOException exception)
        {
            System.out.println("IOException");
        }
    }
}