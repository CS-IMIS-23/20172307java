package eg;
//********************************************************************************************************************
//   Nametag.java               Author:hyt
//
//   Demonstretes the use of commmand line arguments.
//********************************************************************************************************************
public class Nametag {
    //----------------------------------------------------------------------------------------------------------------
    //  Prints a simple name tag using and a name that is
    //  specified by the user.
    //----------------------------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        System.out.println();
        System.out.println("    " + args[0]);
        System.out.println("My name is " + args[1]);
    }
}
