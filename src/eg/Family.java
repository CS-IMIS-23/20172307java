package eg;
//********************************************************************************************************************
//  Family.java                         Author:hyt
//
//  Demonstrates the use of varoable length parameters.
//*********************************************************************************************************************

public class Family {
    private String[] members;

    //----------------------------------------------------------------------------------------------------------------
    // Constructor: Setup this family by storing the possiblr
    // multiple name s that are passed in as parameters.
    //----------------------------------------------------------------------------------------------------------------
    public Family(String ... names)
    {
        members = names;
    }

    //---------------------------------------------------------------------------------------------------------------
    //  Returns a string representation of this family.
    //---------------------------------------------------------------------------------------------------------------

    public String toString() {
        String result = "";

        for (String name : members)
            result += name + "\n";

        return result;
    }
}
