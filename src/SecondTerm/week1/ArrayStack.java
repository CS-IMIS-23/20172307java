package SecondTerm.week1;

import java.util.Arrays;

//---------------------------------------------------------------------------------------------------
//  ArrayStack                              Author:hyt
//  课堂测试，用ArrayList类实现栈
//---------------------------------------------------------------------------------------------------
public class ArrayStack<T>
{
    private final int DEFAULT_CAPACITY = 100;
    private int top;
    private T[] stack;
    public ArrayStack() {
        top = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }
    //push功能
    public void push (T element)
    {
        if (size() == stack.length)
            expandCapacity();
        stack[top] = element;
        top++;
    }
    //扩展容量
    private void expandCapacity(){
        stack = Arrays.copyOf(stack,stack.length*2);
    }
    //pop功能
    public Object pop() throws EmptyCollectionException{
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        top--;
        Object result = stack[top];
        stack[top] = null;
        return result;
    }
    public T peek() throws EmptyCollectionException
    {
        if(isEmpty()){
            throw new EmptyCollectionException("Stack");
        }
        return stack[top - 1];
    }
    public boolean isEmpty()
    {
        if(top == 0){
            return true;
        }
        else{
            return false;
        }
    }
    public int size()
    {
        return top;
    }
    public String toString()
    {
        String result = "";
        for(int num = 0 ;num < size(); num++) {
            result += stack[num] +" ";
        }
        return result;
    }
}

