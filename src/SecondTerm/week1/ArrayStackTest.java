package SecondTerm.week1;
//测试ArrayStack
public class ArrayStackTest {
    public static void main(String[] args) {
        ArrayStack arrayStack= new ArrayStack();
        arrayStack.push(1);
        arrayStack.push(2);
        System.out.println(arrayStack);
        //测试push
        System.out.println("栈内是否为空： "+ arrayStack.isEmpty());
        //测试isEmpty
        arrayStack.pop();
        System.out.println(arrayStack);
        //测试pop
        System.out.println("栈内包含的数据数量： "+ arrayStack.size());
        //测试size
        System.out.println("栈顶元素为："+arrayStack.peek());
        //测试peek
        System.out.println(arrayStack.toString());
        //测试tostring

    }
}
