package SecondTerm.week1;
//---------------------------------------------------------------------------------------
//EmptyCollectionException                       Author:hyt
//栈的异常处理
//----------------------------------------------------------------------------------------
public class EmptyCollectionException extends RuntimeException
{
    public EmptyCollectionException(String collection){
        super("The" + collection + "is empty.");
    }
}