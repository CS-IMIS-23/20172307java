package SecondTerm.week1;
import java.util.Scanner;
import java.util.StringTokenizer;
public class LinkNode {
    public static void main(String[] args) {

        Number head = new Number();
        Scanner scan = new Scanner(System.in);
        System.out.print("输入数字：");
        String lianbiao = scan.nextLine();
        StringTokenizer lian = new StringTokenizer(lianbiao, " ");
        while (lian.hasMoreTokens())
        {
            head.add(Integer.parseInt(lian.nextToken()));
        }
        System.out.println("链表： " + head.toString() + "\n----------------------------");
        //检验插入
        head.insert(2,5);//在第二个位置插入5
        System.out.println("链表： " + head.toString() + "\n----------------------------");
        //检验删除
        head.delete(2);//删除第二个位置
        System.out.println("链表： " + head.toString() + "\n----------------------------");
        //检验排序
        head.sort();
        System.out.println("链表： " + head.toString() + "\n----------------------------");





    }

}
