package SecondTerm.week9;

public class QueueHeap<T> implements Comparable<QueueHeap>{
    private int num;
    private T substance;
    private static int nextOrder = 0;
    public QueueHeap(T substance){
        this.substance = substance;
        num = nextOrder;
        nextOrder++;
    }

    public int getNum() {
        return num;
    }

    public T getSubstance() {
        return substance;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setSubstance(T substance) {
        this.substance = substance;
    }

    @Override
    public String toString() {
        return String.valueOf(substance);
    }

    public int compareTo(QueueHeap queueHeap){
        if (num > queueHeap.getNum())
            return  1;
        else
            return -1;
    }
}