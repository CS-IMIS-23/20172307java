package SecondTerm.week9;

public class QueueHeapMin<T> extends LinkedHeap<QueueHeap<T>> {

    public QueueHeapMin(){
        super();
    }

    public void enqueue(QueueHeap obj) {
        super.addElement(obj);
    }

    public QueueHeap dequeue(){
        return super.removeMin();
    }

    public QueueHeap first(){
        return super.findMin();
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    public int size(){
        return super.size();
    }

    public String toHeapString() {
        return super.toString();
    }

    public String toQueueString(){
        String result = "";
        QueueHeap queueHeap;
        int sum = size();
        for(int a = 0; a < sum; a++){
            queueHeap = dequeue();
            result += queueHeap + " ";
            enqueue(new QueueHeap(queueHeap));
        }
        return result;
    }
}