package SecondTerm.week9;

import SecondTerm.week7.BinaryTreeNode;

public class HeapNode<T> extends BinaryTreeNode<T> {

    protected HeapNode<T>  parent;

    public HeapNode(T obj) {
        super(obj);
        parent = null;
    }

    public HeapNode<T> getParent(){
        return parent;
    }

    public void setElement(T obj){
        element=obj;
    }

    public void setParent(HeapNode<T> node){
        parent = node;
    }
}
