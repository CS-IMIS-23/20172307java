package SecondTerm.week9;

/**
 * HeapSort sorts a given array of Comparable objects using a heap.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class HeapSort<T> {
    /**
     * Sorts the specified array using a Heap
     *
     * @param data
     *            the data to be added to the heapsort
     */
    public void HeapSort(T[] data) {
        ArrayHeap<T> temp = new ArrayHeap<T>();

        // copy the array into a heap
        for (int i = 0; i < data.length; i++)
            temp.addElement(data[i]);

        // place the sorted elements back into the array
        int count = 0;
        while (!(temp.isEmpty())) {
            data[count] = temp.removeMin();
            count++;
        }
    }

    public static void main(String[] args) {
        Integer[] data = {3,1,2,5,8,7,6,9,0,4};
        HeapSort hs = new HeapSort<Integer>();
        hs.HeapSort(data);
        for(int i=0;i<data.length;i++){
            System.out.println(data[i]);
        }

    }
}