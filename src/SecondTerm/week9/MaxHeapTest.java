package SecondTerm.week9;

public class MaxHeapTest {public static void main(String[] args) {
    MaxHeap arrayHeap = new MaxHeap();
    int[] array = {36,30,18,40,32,45,22,50};
    Object[] obj = new Object[array.length];
    for(int a = 0; a < array.length; a++){
        arrayHeap.addElement(array[a]);
    }

    System.out.println("大顶堆的层序输出：" + arrayHeap.toString());

    for(int n = 1; n <= array.length; n++){
        obj[n - 1] = arrayHeap.removeMax();
        System.out.println("第" + n + "次:" + arrayHeap);
    }
    System.out.print("排序结果： ");
    String result = "";
    for(int m = 0; m < 8; m++){
        result += obj[m] + " ";
    }
    System.out.println(result);
}
}

