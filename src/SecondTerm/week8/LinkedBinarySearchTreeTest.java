package SecondTerm.week8;

public class LinkedBinarySearchTreeTest {
    public static void main(String argsp[]){
        LinkedBinarySearchTree<Integer> nodes = new LinkedBinarySearchTree(2);
        nodes.addElement(4);
        nodes.addElement(8);
        nodes.addElement(201);
        nodes.addElement(1);
        nodes.addElement(956);

        System.out.println("二叉查找树：\n" + nodes.toString());
        //findMax
        System.out.println("该树的最大元素: " + nodes.findMax());
        //removeMax
        nodes.removeMax();
        System.out.println("删除最大元素后：" + nodes.toString());
        //findMin
        System.out.println("该树的最小元素：" + nodes.findMin());
        //removeMin
        nodes.removeMin();
        System.out.println("删除最小元素后：" + nodes.toString());

    }
}