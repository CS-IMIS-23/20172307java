package SecondTerm.week8;

public interface BinarySearchTreeADT<T> extends BinaryTreeADT<T>{

    public void addElement(T element);
    public T removeElement(T element);
    public void removeAllOccurrences(T targetElement);
    public T removeMin();
    public T removeMax();
    public T findMin();
    public T findMax();
}