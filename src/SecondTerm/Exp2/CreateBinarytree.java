package SecondTerm.Exp2;
import SecondTerm.week7.LinkedBinaryTree;

import java.util.Scanner;

public class CreateBinarytree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("输入中序排列");
        String InOrder = scanner.nextLine();
        System.out.println("输入后序排列");
        String PreOrder = scanner.nextLine();

        char[] In = InOrder.toCharArray();
        char[] Pre = PreOrder.toCharArray();


        LinkedBinaryTree tree = Create(In,Pre);
        System.out.println(tree.toString());


    }
//用递归的方式实现
    public static LinkedBinaryTree Create(char[] in, char[] pre)
    {
        LinkedBinaryTree tree;
        if(pre.length == 0 || in.length == 0 || pre.length != in.length){
            tree =  new LinkedBinaryTree();
        }
        else {
            int x = 0;
            while (in[x] != pre[0]) {
                x++;
            }

            char[] inLeft = new char[x];
            char[] preLeft = new char[x];
            char[] inRight = new char[in.length - x - 1];
            char[] preRight = new char[pre.length - x - 1];

            for (int y = 0; y < in.length; y++) {
                if (y < x) {
                    inLeft[y] = in[y];
                    preLeft[y] = pre[y + 1];
                } else if (y > x) {
                    inRight[y - x - 1] = in[y];
                    preRight[y - x - 1] = pre[y];
                }
            }
            LinkedBinaryTree left = Create(inLeft, preLeft);
            LinkedBinaryTree right = Create(inRight, preRight);
            tree = new LinkedBinaryTree(pre[0], left,right);
        }
        return tree;
    }
}