package SecondTerm.Exp2;

import SecondTerm.week7.LinkedBinaryTree;

import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;

public class ChangeTree  extends LinkedBinaryTree {
    public Stack<LinkedBinaryTree> operand;
    //存放操作数
    public Stack<String> operator;
    //存放运算符
    public String result = "";

    public ChangeTree()
    {
        operand = new Stack<LinkedBinaryTree>();
        operator = new Stack<String>();
    }

    public LinkedBinaryTree getOperand(Stack<LinkedBinaryTree> treeStack)
    {
        LinkedBinaryTree temp = treeStack.pop();

        return temp;
    }

    public String convert(String infixline) {
        LinkedBinaryTree operand1;
        LinkedBinaryTree operand2;

        StringTokenizer stringTokenizer = new StringTokenizer(infixline);

        String tempvalue;

        while (stringTokenizer.hasMoreTokens()) {
            tempvalue = stringTokenizer.nextToken();


            if (tempvalue.equals("(")) {
                operator.push(tempvalue);
                tempvalue = stringTokenizer.nextToken();

                while (!tempvalue.equals(")")) {
                    if (tempvalue.equals("+") || tempvalue.equals("-")) {
                        if (!operator.isEmpty()) {
                            if (operator.peek().equals("("))
                                operator.push(tempvalue);
                            else {
                                String b = operator.pop();
                                operand1 = getOperand(operand);
                                operand2 = getOperand(operand);
                                LinkedBinaryTree a = new LinkedBinaryTree(b, operand2, operand1);
                                operand.push(a);
                                operator.push(tempvalue);
                            }
                        } else {
                            operator.push(tempvalue);
                        }
                    } else if (tempvalue.equals("*") || tempvalue.equals("/")) {
                        if (!operator.isEmpty()) {
                            if (operator.peek().equals("*") || operator.peek().equals("/")) {
                                String b = operator.pop();
                                operand1 = getOperand(operand);
                                operand2 = getOperand(operand);
                                LinkedBinaryTree a = new LinkedBinaryTree(b, operand2, operand1);
                                operand.push(a);
                                operator.push(tempvalue);
                            } else {
                                operator.push(tempvalue);
                            }
                        }
                    } else {
                        LinkedBinaryTree a = new LinkedBinaryTree(tempvalue);
                        operand.push(a);
                    }
                    tempvalue = stringTokenizer.nextToken();
                }

                while (true) {
                    String b = operator.pop();
                    if (!b.equals("(")) {
                        operand1 = getOperand(operand);
                        operand2 = getOperand(operand);
                        LinkedBinaryTree a = new LinkedBinaryTree(b, operand2, operand1);
                        operand.push(a);
                    }
                    else {
                        break;
                    }
                }
            }
            else if (tempvalue.equals("+") || tempvalue.equals("-")) {
                if (!operator.isEmpty()) {
                    String b =  operator.pop();
                    operand1 = getOperand(operand);
                    operand2 = getOperand(operand);
                    LinkedBinaryTree a = new LinkedBinaryTree(b, operand2, operand1);
                    operand.push(a);
                    operator.push(tempvalue);
                } else {
                    operator.push(tempvalue);
                }
            } else if (tempvalue.equals("*") || tempvalue.equals("/")) {
                if (!operator.isEmpty()) {
                    if (operator.peek().equals("*") || operator.peek().equals("/"))
                    {
                        String b = operator.pop();
                        operand1 = getOperand(operand);
                        operand2 = getOperand(operand);
                        LinkedBinaryTree a = new LinkedBinaryTree(b, operand2, operand1);
                        operand.push(a);
                        operator.push(tempvalue);
                    }
                    else
                    {
                        operator.push(tempvalue);
                    }
                }else {
                    operator.push(tempvalue);
                }
            }
            else
            {
                LinkedBinaryTree a = new LinkedBinaryTree(tempvalue);
                operand.push(a);
            }
        }

        while (!operand.isEmpty() && !operator.isEmpty()) {
            String b = operator.pop();
            operand1 = getOperand(operand);
            operand2 = getOperand(operand);
            LinkedBinaryTree a = new LinkedBinaryTree(b, operand2, operand1);
            operand.push(a);
        }
        System.out.println("前缀表达式为："+infixline);
        System.out.println("输出后缀表达式树：");
        System.out.println(operand.peek().toString());
        Iterator postfixTree = operand.pop().iteratorPostOrder();


        System.out.println("对应的后缀表达式为：");
        while (postfixTree.hasNext()) {
            result += postfixTree.next()+" ";
        }
        return result;
    }

}


