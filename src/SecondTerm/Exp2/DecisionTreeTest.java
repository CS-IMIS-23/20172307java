package SecondTerm.Exp2;

import SecondTerm.week7.DecisionTree;

import java.io.FileNotFoundException;

public class DecisionTreeTest {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("请做出判断  .");
        SecondTerm.week7.DecisionTree decisionTree = new DecisionTree("input.txt");
        decisionTree.evaluate();

        System.out.println("深度：" + decisionTree.getHeight());
        System.out.println("叶结点数目：" + decisionTree.getNumber());
        System.out.println("层序输出(递归)：");
        decisionTree.getString1();
        System.out.println("层序输出(非递归)：");
        decisionTree.getString2();
    }
}
