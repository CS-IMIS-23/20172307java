package SecondTerm.Exp2;

import SecondTerm.week2.PostfixEvaluator;

public class ChangeTreeTest {
    public static void main(String[] args) {
        ChangeTree tree = new ChangeTree();
        String m = "2 + ( 4 + 3 ) * 4 - 5";
        String n= tree.convert(m);
        System.out.println(n);
        PostfixEvaluator postfixEvaluator = new PostfixEvaluator();
        System.out.println("后缀表达式求值得：" + postfixEvaluator.evaluate(n));
    }
}
