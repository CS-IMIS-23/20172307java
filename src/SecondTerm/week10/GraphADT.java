package SecondTerm.week10;

import java.util.Iterator;

public interface GraphADT<T> {

    public void addVertex(T vertex);

    public void removeVertex(T vertex);

    public void addEdge(T v1,T v2);

    public void removeEdge(T v1,T v2);
    /**
     * 广度遍历
     * @param startIndex
     * @return
     */
    public Iterator iteratorBFS(T startIndex);
    /**
     * 深度遍历
     * @param startIndex
     * @return
     */
    public Iterator iteratorDFS(T startIndex);
    /**
     * 两个顶点之间最短路径的获取
     * @param startVertex
     * @param targetVertex
     * @return
     */
    public Iterator iteratorShortestPath(T startVertex,T targetVertex);
    /**
     * 判定是否为空
     * @return
     */
    public boolean isEmpty();
    /**
     * 测试连通性
     * @return
     */
    public boolean isConnected();

    /**
     * 返回顶点的规模
     * @return
     */
    public int size();
    /**
     * 输出图结构
     * @return
     */
    public String toString();
}