package SecondTerm.week10;

import java.util.Iterator;

public class GraphTest {
    public static void main(String[] args) {
        Graph<String> graph = new Graph<String>();
        graph.addVertex("1");
        graph.addVertex("2");
        graph.addVertex("3");
        graph.addVertex("4");
        graph.addVertex("6");
        graph.addEdge("1", "2");
        graph.addEdge("2", "3");
        graph.addEdge("2", "4");
        graph.addEdge("6", "1");
        Iterator<String> iterator1 = graph.iteratorBFS(0);
        String result1 = "";
        for (Iterator<String> it = iterator1; it.hasNext(); ) {
            String i = it.next();
            result1 += i + " ";
        }
        System.out.println("广度优先遍历：" + result1);
        Iterator<String> iterator2 = graph.iteratorDFS(0);
        String result2 = "";
        for (Iterator<String> it = iterator2; it.hasNext(); ) {
            String i = it.next();
            result2 += i + " ";
        }
        System.out.println("深度优先遍历：" + result2);

        graph.removeVertex("6");
        System.out.println("6到2的最短距离：" + graph.shortestPathLength("6", "2"));
        System.out.println(graph.toString());
    }
}
