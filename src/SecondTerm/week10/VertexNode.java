package SecondTerm.week10;

public class VertexNode<T> {
    private T element;
    private VertexNode<T> next;
    public VertexNode(T element)
    {
        this.element = element;
        next = null;
    }
    public void setNext(VertexNode<T> vertex){
        next = vertex;
    }
    public VertexNode<T> getNext(){
        return next;
    }
    public T getElement(){
        return element;
    }
}