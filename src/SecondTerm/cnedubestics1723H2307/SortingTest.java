package SecondTerm.cnedubestics1723H2307;

public class SortingTest {
    public static void main(String[] args) {

        //二叉树排序测试
        Integer[] num1 = {2, 0, 1, 7, 23, 7};
        String result1 = "";
        Sorting.BinaryTreeSort(num1);
        for (int i = 0; i < num1.length; i++) {
            result1 += num1[i] + " ";
        }
        System.out.println("数组二叉树排序结果：" + result1);

        //选择排序测试
        Integer[] num2 = {2, 0, 1, 7, 23, 7};
        String result2 = "";
        Sorting.selectionSort1(num2);
        for (int i = 0; i < num2.length; i++) {
            result2 += num2[i] + " ";
        }
        System.out.println("选择排序结果：" + result2);

        //插入排序测试
        Integer[] num3 = {2, 0, 1, 7, 23, 7};
        String result3 = "";
        Sorting.insertionSort(num3);
        for (int i = 0; i < num3.length; i++) {
            result3 += num3[i] + " ";
        }
        System.out.println("插入排序结果：" + result3);

    }
}