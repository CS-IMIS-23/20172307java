package SecondTerm.cnedubestics1723H2307;

public class SearchingTest {
    public static void main(String[] args) {
        int []a={2,0,4,7,23,0,7};
        System.out.println("二分查找（有的话返回索引值）："+Searching.BinarySearch(a,7,7));
        System.out.println("插值查找是否有24："+Searching.InsertionSearch(a,7));
        System.out.println("斐波那契查找是否有2（有的话返回索引值）："+Searching.FibonacciSearch(a,7,2));
        System.out.println("树表查找是否有4："+Searching.TreeSearch(a,23));
        System.out.println("哈希查找是否有23（有的话返回索引值）:"+Searching.hashsearch(a,23));
        System.out.println("分块查找是否有23："+Searching.BlockSearch(a,23,new int[]{10,20,70}));
        System.out.println("分块查找是否有17："+Searching.BlockSearch(a,17,new int[]{10,20,70}));

    }
}


