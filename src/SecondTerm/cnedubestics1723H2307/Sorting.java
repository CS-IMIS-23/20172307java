package SecondTerm.cnedubestics1723H2307;

import SecondTerm.week8.LinkedBinarySearchTree;
import SecondTerm.week9.LinkedHeap;

import java.util.Iterator;

public class Sorting {
    //选择排序
    public static<T extends Comparable<? super T>> String selectionSort(T[] data){
        int min;
        T temp;
        for (int index = 0; index < data.length-1;index++){
            min =index;
            for (int scan = index + 1;scan<data.length;scan++)
                if (data[scan].compareTo(data[min])<0)
                    min = scan;

            temp = data[min];
            data[min] = data[index];
            data[index] = temp;
        }
        String a = "";
        for (int n=0;n<=data.length-2;n++){
            a+=data[n]+",";
        }
        a+=data[data.length-1];
        return a;
    }
    public static<T extends Comparable<? super T>> String selectionSort1(T[] data){
        int max;
        T temp;
        for (int index = 0; index < data.length-1;index++){
            max =index;
            for (int scan = index + 1;scan<data.length;scan++)
                if (data[scan].compareTo(data[max])>0)
                    max = scan;

            temp = data[max];
            data[max] = data[index];
            data[index] = temp;
        }
        String a = "";
        for (int n=0;n<=data.length-2;n++){
            a+=data[n]+",";
        }
        a+=data[data.length-1];
        return a;
    }
    //堆排序
    public static <T extends Comparable<T>>void HeapSort(T[] data)
    {
        LinkedHeap<T> linkedHeap = new LinkedHeap<>();
        for (int i=0;i<data.length;i++)
            linkedHeap.addElement(data[i]);
        for (int i=0;i<data.length;i++)
            data[i] = linkedHeap.removeMin();
    }
    //二叉树排序
    public static <T extends Comparable<T>>void BinaryTreeSort(T[] data)
    {
        LinkedBinarySearchTree<T> linkedBinarySearchTree = new LinkedBinarySearchTree<>();
        for (int i=0;i<data.length;i++)
            linkedBinarySearchTree.addElement(data[i]);

        Iterator iterator = linkedBinarySearchTree.iteratorInOrder();
        int index = 0;
        while (iterator.hasNext())
        {
            data[index] = (T) iterator.next();
            index++;
        }
    }
    //插入排序
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;

            // shift larger values to the right
            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
            }

            data[position] = key;
        }
    }
    //快速排序
    public static <T extends Comparable<T>> void mergeSort(T[] data)
    {
        mergeSort(data, 0, data.length - 1);
    }

    private static <T extends Comparable<T>> void mergeSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);
        }
    }
    private static <T extends Comparable<T>> void merge(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;

        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }

        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
        }

        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++)
            data[index] = temp[index];
    }



}
