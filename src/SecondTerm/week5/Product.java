package SecondTerm.week5;

public class Product implements Comparable<Product> {
    protected String name;
    protected int price;
    protected int number;

    public Product(String Name,int Price,int Number){
        name = Name;
        price = Price;
        number = Number;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public int compareTo(Product o) {
        int result;
        result = name.compareTo(o.getName());
        return result;
    }

    @Override
    public String toString() {
        String result = "商品名称： "+ name + "\t" + "商品单价： "+ price + "\t" + "商品编号： "+ number;
        return result;
    }
}

