package SecondTerm.week5;

import SecondTerm.week2.EmptyCollectionException;
import SecondTerm.week4.ElementNotFoundException;
public class ProductList   {
    protected int count;
    protected LinearNode<Product> head;
    protected LinearNode<Product> tail;
    protected int modCount;

    public ProductList() {
        count = 0;
        head = tail = null;
        modCount = 0;
    }

    //删除
    public Product remove(Product target) {
        if (isEmpty())
            throw new EmptyCollectionException("ProductList");
        boolean found = false;
        LinearNode<Product> previous = null;
        LinearNode<Product> current = head;
        while (current != null && !found)
            if (target.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }
        if (!found)
            throw new ElementNotFoundException("ProductList");

        if (size() == 1)
            head = tail = null;
        else if (current.equals(head))
            head = current.getNext();
        else if (current.equals(tail))
        {
            tail = previous;
            tail.setNext(null);
        } else
            previous.setNext(current.getNext());

        count--;
        return current.getElement();
    }


    //添加
    public void add(Product element) {
        LinearNode<Product> node = new LinearNode<Product>(element);
        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    //插入
    public void addAfter(Product target, Product newProduct) {
        LinearNode<Product> node = new LinearNode<Product>(newProduct);
        LinearNode<Product> temp = head;
        if (temp == null) {
            head = tail = node;
        }
        while ((temp != null) && !(temp.getElement().equals(target))) {
            temp = temp.getNext();
        }
        if (temp.getNext() == null) {
            temp.setNext(node);
            tail = node;
        } else {
            node.setNext(temp.getNext());
            temp.setNext(node);
        }
        count++;
    }

    //查找
    public Product find(String target) {
        LinearNode<Product> temp = head;
        while (temp != null && !temp.getElement().getName().equals(target)) {
            temp = temp.getNext();
        }
        return temp.getElement();
    }

    //选择排序
    public void SelectSorting() {
        LinearNode<Product> min;
        Product temp;
        LinearNode<Product> numNode = head;
        while (numNode != null) {
            min = numNode;
            LinearNode<Product> current = min.next;
            while (current != null) {
                if (current.getElement().compareTo(min.getElement()) < 0) {
                    min = current;
                }
                current = current.next;
            }
            temp = min.getElement();
            min.setElement(numNode.getElement());
            numNode.setElement(temp);
            numNode = numNode.next;
        }
    }
    public boolean isEmpty(){
        return count == 0;
    }
    public int size(){
        return count;
    }
    @Override
    public String toString()
    {
        LinearNode<Product> temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement()+ " "+"\n";
            temp = temp.getNext();
        }
        return result;
    }
}
