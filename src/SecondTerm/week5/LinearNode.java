package SecondTerm.week5;

public class LinearNode<T> {
    protected LinearNode<T> next;
    protected T element;

    public LinearNode(){
        next=null;
        element=null;
    }
    public LinearNode(T elem)
    {
        next = null;
        element = elem;
    }

    public LinearNode<T> getNext() {
        return next;
    }
    public void setNext(LinearNode node){
        next = node;
    }
    public T getElement(){
        return element;
    }

    public void setElement(T elem) {
        element=elem;
    }
}