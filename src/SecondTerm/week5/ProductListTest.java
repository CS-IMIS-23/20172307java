package SecondTerm.week5;

public class ProductListTest {
    public static void main(String[] args) {
        Product pro1 = new Product("可乐", 3, 1);
        Product pro2 = new Product("薯片", 4, 2);
        Product pro3 = new Product("豆干", 6, 3);
        Product pro4 = new Product("卤蛋", 2, 4);
        Product pro5 = new Product("饼干", 8, 5);

        ProductList List = new ProductList();
        List.add(pro1);
        List.add(pro2);
        List.add(pro3);
        List.add(pro4);
        List.add(pro5);
        System.out.println("添加后列表显示为： "+"\n"+ List.toString());
        List.remove(pro3);
        System.out.println("删除后列表显示为： "+"\n"+ List.toString());
        List.addAfter(pro2,pro3);
        System.out.println("插入后列表显示为： "+"\n"+ List.toString());
        System.out.println("查找结果： "+"\n"+ List.find("可乐"));
        List.SelectSorting();
        System.out.println("排序后列表显示为： "+"\n"+ List.toString());
    }

 }
