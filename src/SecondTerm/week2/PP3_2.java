package SecondTerm.week2;
/*week2 homework PP3.2
Author:hyt
 */

import java.util.Scanner;
import java.util.StringTokenizer;

public class PP3_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入一个句子： ");
        String s = scanner.nextLine();
        reversal(s);

    }
    static void reversal(String string){
        ArrayStack stack = new ArrayStack();
        StringTokenizer to = new StringTokenizer(string);
        while (to.hasMoreTokens()){
            String rever = to.nextToken();
            for (int n = 0;n < rever.length();n++)
            {
                stack.push(rever.charAt(n));
            }
            int j = 0;
            String s = "";
            while (j < rever.length()){
                s += stack.pop() + "";
                j++;
            }
            System.out.print(s + " ");
        }
    }
}
