package SecondTerm.week2;

public class LindedStack<T> implements StackADT<T> {
    private int count;
    private LinearNode<T> top;
    public LindedStack(){
        count = 0;
        top = null;
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp=new LinearNode<T>(element);

        temp.setNext(top);
        top = temp;
        count ++;
    }

    @Override
    public void expandCapacity() {

    }

    @Override
    public T pop() throws EmptyCollectionException {

        if (isEmpty()) {
            throw new EmptyCollectionException("the stack is empty!");
        }

        T result = top.getElement();
        top=top.getNext();
        count--;

        return result;
    }

    @Override
    public T peek() {

        if (isEmpty()) {
            throw new EmptyCollectionException("the stack is empty!");
        }
        T result = top.getElement();
        top=top.getNext();

        return result;
    }

    @Override
    public boolean isEmpty() {

        boolean p ;
        if (size()==0){
            p=true;
        }
        else
        {
            p=false;
        }
        return p;
    }

    @Override
    public int size() {

        return count;
    }
    @Override
    public String toString(){
        LinearNode node = new LinearNode();
        while (node != null) {
            System.out.print(node.getElement()+" ");
            node = node.getNext();
        }

        return node.getElement().toString();
    }
}