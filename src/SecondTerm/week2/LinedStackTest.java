package SecondTerm.week2;

public class LinedStackTest {
    public static void main(String[] args) {
        LindedStack lindedStack= new LindedStack();
        lindedStack.push(1);
        lindedStack.push(2);
        System.out.println("栈内元素为： " + lindedStack.toString());
        //测试push和tostring
        System.out.println("栈内是否为空： "+ lindedStack.isEmpty());
        //测试isEmpty
        lindedStack.pop();
        System.out.println("栈内元素为： "+lindedStack.toString());
        //测试pop
        System.out.println("栈内包含的数据数量： "+ lindedStack.size());
        //测试size
        System.out.println("栈顶元素为："+lindedStack.peek());
        //测试peek

    }
}
