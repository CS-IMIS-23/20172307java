package SecondTerm.week2;

import java.util.Scanner;

public class PostfixTester {
    public static void main(String args[]){
        String expression, again;
        int result;

        Scanner in = new Scanner(System.in);

        do{
            PostfixEvaluator evaluatorr = new PostfixEvaluator();
            System.out.println("Enter a valid post-fix expression one token " + "at a time with a space between each token(e.g.  7 2 + 3 0 7 - + *)");
            System.out.println("Each token must be an integer or an operator (+,-,*,/)");
            expression = in.nextLine();

            result = evaluatorr.evaluate(expression);
            System.out.println();
            System.out.println("That expression equals " + result);

            System.out.print("Evaluate another expression [Y/N]?");
            again = in.nextLine();
            System.out.println();
        }while(again.equalsIgnoreCase("y"));
    }
}