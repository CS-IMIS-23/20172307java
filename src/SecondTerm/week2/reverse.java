package SecondTerm.week2;

import java.util.Scanner;
import java.util.StringTokenizer;

/*
week2 homework PP3.8
Author:hyr
 */
public class reverse {
    public static void main(String[] args) {
        String s,t;
        Scanner scanner = new Scanner(System.in);
        System.out.print("输入一些元素: ");
        s = scanner.nextLine();

        StringTokenizer tokenizer = new StringTokenizer(s);
        ArrayStack stack = new ArrayStack();
        for (int a = 0;a <s.length();a++){
            while (tokenizer.hasMoreTokens()) {
                t = tokenizer.nextToken();
                stack.push(t);
            }
        }


        String reversal = "倒叙为： ";
        int n = stack.size();
        for (int m = 0;m < n;m++){
            reversal += stack.pop() + " ";
        }
        System.out.println(reversal);
    }
}


