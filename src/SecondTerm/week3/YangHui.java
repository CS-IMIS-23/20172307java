package SecondTerm.week3;

import java.util.Scanner;

public class YangHui {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入行数： ");
        int line = scanner.nextInt();
        int[] n = new int[line + 1];
        int pre = 1;
        for(int i = 1;i<=line;i++){
            for (int j = 1;j<=i;j++){
                int current = n[j];
                n[j] = pre + current;
                System.out.print(n[j] + "");
            }
            System.out.println();
        }

    }
}
