package SecondTerm.week3;

import SecondTerm.week2.EmptyCollectionException;
import SecondTerm.week3.jsjf.QueueADT;

public class ArrayQueue<T> implements QueueADT<T> {
    private final int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;
    /**
     * Creatas an empty queue using the specified capacity.
     * @param initialCapacity the initial size of the circular array queue
     */
    public ArrayQueue(int initialCapacity){
        front = rear = count = 0;
        queue = ((T[])(new Object[initialCapacity]));
    }
    /**
     * Creates an empty queue using the default capacity.
     */
    public ArrayQueue(){
        front = rear = count = 0;
        queue = ((T[])(new Object[DEFAULT_CAPACITY]));
    }

    @Override
    public void enqueue(T element) {
        if(size() == queue.length)
            expandCapcity();

        queue[rear] = element;
        rear = (rear + 1) % queue.length;
        count++;
    }

    /**
     * Create a new array to store the contents of this queue with twice the capacity of the old one.
     */
    public void expandCapcity(){
        T[] larger = (T[])(new Object[queue.length * 2]);
        for(int scan = 0; scan < count; scan++){
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }
        front = 0;
        rear = count;
        queue = larger;
    }

    /**
     * Removes the element at the front of the queue and returns a reference to it.
     * @return the element removed from the front of the queue
     * @throws an EmptyCollectionException if the queue is empty.
     */
    @Override
    public T dequeue() throws EmptyCollectionException {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;

        return result;
    }

    @Override
    public T first() {
        if(isEmpty())
            throw new EmptyCollectionException("queue");

        return queue[front];
    }

    @Override
    public boolean isEmpty() {
        if(front == rear)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result ="";
        int temp = front;
        for(int num = 0; num < size(); num++) {
            result += queue[temp] + " ";
            temp = (temp + 1) % queue.length;
        }
        return result;
    }
}
