package SecondTerm.week7;

import java.util.Iterator;

public interface BinaryTreeADT<T> {

    /**
     * 获取根部元素
     *
     * @return
     */
    public T getRootElement();

    /**
     * 判断是否是空树
     *
     * @return true if this binary tree is empty, false otherwise
     */
    public boolean isEmpty();

    /**
     * 返回树中元素的个数
     *
     * @return the number of elements in the tree
     */
    public int size();

    /**
     *判定二叉树中是否存在当前的元素
     *
     * @param targetElement
     *            the element being sought in the tree
     * @return true if the tree contains the target element
     */
    public boolean contains(T targetElement);

    /**
     *在二叉树中查找响应的元素
     *
     * @param targetElement
     *            the element being sought in the tree
     * @return a reference to the specified element
     */
    public T find(T targetElement);

    /**
     *输出二叉树
     *
     * @return a string representation of the binary tree
     */
    public String toString();

    /**
     *返回二叉树的迭代器
     *
     * @return an iterator over the elements of this binary tree
     */
    public Iterator<T> iterator();

    /**
     * Returns an iterator that represents an inorder traversal on this binary
     * tree.
     *
     * @return an iterator over the elements of this binary tree
     */
    public Iterator<T> iteratorInOrder();

    /**
     * Returns an iterator that represents a preorder traversal on this binary
     * tree.
     *
     * @return an iterator over the elements of this binary tree
     */
    public Iterator<T> iteratorPreOrder();

    /**
     * Returns an iterator that represents a postorder traversal on this binary
     * tree.
     *
     * @return an iterator over the elements of this binary tree
     */
    public Iterator<T> iteratorPostOrder();

    /**
     * Returns an iterator that represents a levelorder traversal on the binary
     * tree.
     *
     * @return an iterator over the elements of this binary tree
     */
    public Iterator<T> iteratorLevelOrder();

}