package SecondTerm.week7;

public class LinkedBinaryTreeTest {
    public static void main(String args[]){
        LinkedBinaryTree<String> Tree1 = new LinkedBinaryTree<>("20172307");
        LinkedBinaryTree<String> Tree2 = new LinkedBinaryTree<>("黄宇瑭");
        LinkedBinaryTree<String> Tree3 = new LinkedBinaryTree<>("网络空间安全系");


        LinkedBinaryTree node1 = new LinkedBinaryTree("信息管理与信息系统",Tree1, Tree2);
        LinkedBinaryTree node2 = new LinkedBinaryTree("浙江省",Tree3,node1);

        //前序输出
        System.out.println("前序输出：");
        node2.toPreString();
        //后序输出
        System.out.println("\n后序输出：");
        node2.toPostString();
        //contains
        System.out.println("此树是否含有‘20172307’" + node2.contains("20172307"));


    }
}