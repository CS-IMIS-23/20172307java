package SecondTerm.week7;

public class HashNode {
    private HashNode next;
    private int ele;
    public HashNode(int elem)
    {
        ele = elem;
        next = null;
    }

    public HashNode getNext() {
        return next;
    }

    public void setNext(HashNode next) {
        this.next = next;
    }

    public int getEle() {
        return ele;
    }
}