package SecondTerm.week7;

public class ExpressionTreeOp {

    private int termType;
    private char operator;
    private int value;

    public ExpressionTreeOp() {

    }

    public ExpressionTreeOp(int termType, char operator, int value) {
        super();
        this.termType = termType;
        this.operator = operator;
        this.value = value;
    }

    public boolean isOperator() {
        return (termType == 1);
    }

    public char getOperator() {
        return operator;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        if(termType == 1)
            return operator + "";
        else
            return value + "";
    }
}
