package SecondTerm.week7;

public class HashSearch {
    int m;  // m是除数
    int count = 0;
    HashNode[] list ; // 地址存放
    int times = 0;
    int conflictTimes = 0;

    public HashSearch(int m)
    {
        this.m = m;
        list = new HashNode[m];
    }

    public int Remainder(int num)
    {
        int a;
        a = num % m;
        return a;
    }

//将元素添加进链表中的指定位置
    public void addInAddress(int[] ele)
    {

        int x = 0;
        int temp;

        while (x < ele.length)
        {
            temp = Remainder(ele[x]);
            HashNode node = new HashNode(ele[x]);
            if (list[temp] == null)
            {
                list[temp] = node;
            }
            else
            {
                conflictTimes++;
                HashNode current = list[temp];
                while (current.getNext()!= null)
                {
                    current = current.getNext();
                    conflictTimes++;
                }
                current.setNext(node);
            }
            x++;
            times++;
            count++;
        }

    }

    public void ASL()
    {
        double ASL = 0;
        ASL = (double) (times + conflictTimes)/count;
        System.out.println("冲突次数： " + conflictTimes);
        System.out.println("ASL： " + ASL);
    }
}
