package SecondTerm.week7;

import java.io.FileNotFoundException;

public class BackPainAnalyzer {

    /**
     * Asks questions of the user to diagnose a medical problem.
     */
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("So, you're having back  .");
        DecisionTree decisionTree = new DecisionTree("input.txt");
        decisionTree.evaluate();

        System.out.println("深度：" + decisionTree.getHeight());
        System.out.println("叶结点数目：" + decisionTree.getNumber());
        System.out.println("层序输出(递归)：");
        decisionTree.getString1();
        System.out.println("层序输出(非递归)：");
        decisionTree.getString2();

    }
}