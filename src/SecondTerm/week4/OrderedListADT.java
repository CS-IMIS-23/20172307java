package SecondTerm.week4;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月17日 下午3:30:27
 * @description 有序列表接口
 */
public interface OrderedListADT<T> extends ListADT<T> {

    public void add(T element);
}
