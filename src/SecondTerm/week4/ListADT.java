package SecondTerm.week4;

import java.util.Iterator;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月17日 下午2:27:35
 * @description
 *  注意这里是接口继承
 */
public interface ListADT<T> extends Iterable<T>{
    public T removeFirst();
    public T removeLast();
    public T remove(T element);
    public T first();
    public T last();
    public boolean contains(T target);
    public boolean isEmpty();
    public int size();
    public Iterator<T> iterator();
    public String toString();
}