package SecondTerm.week4;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月17日 下午3:31:10
 * @description 类说明
 */
public interface UnorderedListADT<T> extends Iterable<T> {
    public void addToFront(T element);

    public void addToRear(T element);

    public void addAfter(T element, T target);

    public T removeFirst();
}