package SecondTerm.week4;
public class ArrayUnorderedList<T> extends ArrayList<T> implements UnorderedListADT<T> {
    @Override
    public void addToFront(T element) {
        if (size() == list.length)
            expandCapacity();
        for (int shift = rear; shift > 0; shift--)
            list[shift] = list[shift - 1];
        list[0] = element;

        rear++;
        modCount++;
    }
    @Override
    public void addToRear(T element) {
        if (size() == list.length)
            expandCapacity();
        list[rear] = element;

        rear++;
        modCount++;
    }
    @Override
    public void addAfter(T element, T target) {
        if (size() == list.length)
            expandCapacity();
        int scan = 0;
        while (scan < rear && !target.equals(list[scan]))
            scan++;
        if (scan == rear)
            throw new ElementNotFoundException("UnorderedList");
        scan++;
        for (int shift = rear; shift > scan; shift--)
            list[shift] = list[shift - 1];
        list[scan] = element;

        rear++;
        modCount++;
    }
}