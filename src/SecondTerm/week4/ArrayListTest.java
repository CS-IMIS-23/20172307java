package SecondTerm.week4;

public class ArrayListTest {
    public static void main(String args[]){
        ArrayUnorderedList arrayUnorderedList = new ArrayUnorderedList();
        arrayUnorderedList.addToRear("1.20172307黄宇瑭");
        arrayUnorderedList.addToRear("2.网络空间安全系");
        arrayUnorderedList.addToRear("3.2系3班");
        arrayUnorderedList.addToRear("4.浙江省");
        System.out.println(arrayUnorderedList.toString());
        System.out.println("列表头部：" + arrayUnorderedList.first());
        System.out.println("列表尾部：" + arrayUnorderedList.last());
        System.out.println("列表是否为空：" + arrayUnorderedList.isEmpty());
        System.out.println("列表的元素数目：" + arrayUnorderedList.size());
        System.out.println("列表删除头部元素：" + arrayUnorderedList.removeFirst() + "\t" + arrayUnorderedList.first());
        System.out.println("列表删除尾部元素：" + arrayUnorderedList.removeLast() + "\t" + arrayUnorderedList.last());
        System.out.println("列表内是否含有元素'2.网络空间安全系'：" + arrayUnorderedList.contains("2.网络空间安全系"));
        System.out.println("列表内删除元素'2.网络空间安全系'：" + arrayUnorderedList.remove("2.网络空间安全系"));
        System.out.println(arrayUnorderedList.toString());
    }
}