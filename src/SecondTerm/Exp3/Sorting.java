package SecondTerm.Exp3;

public class Sorting {
    public static<T extends Comparable<? super T>> String selectionSort(T[] data){
        int min;
        T temp;
        for (int index = 0; index < data.length-1;index++){
            min =index;
            for (int scan = index + 1;scan<data.length;scan++)
                if (data[scan].compareTo(data[min])<0)
                    min = scan;

            temp = data[min];
            data[min] = data[index];
            data[index] = temp;
        }
        String a = "";
        for (int n=0;n<=data.length-2;n++){
            a+=data[n]+",";
        }
        a+=data[data.length-1];
        return a;
    }
    public static<T extends Comparable<? super T>> String selectionSort1(T[] data){
        int max;
        T temp;
        for (int index = 0; index < data.length-1;index++){
            max =index;
            for (int scan = index + 1;scan<data.length;scan++)
                if (data[scan].compareTo(data[max])>0)
                    max = scan;

            temp = data[max];
            data[max] = data[index];
            data[index] = temp;
        }
        String a = "";
        for (int n=0;n<=data.length-2;n++){
            a+=data[n]+",";
        }
        a+=data[data.length-1];
        return a;
    }
}
