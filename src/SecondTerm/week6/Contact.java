package SecondTerm.week6;

/**
 * Contact represents a phone contact.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class Contact implements Comparable<Contact> {

    private String firstName, lastName, phone;

    /**
     * Sets up this contact with the specified information.
     *
     * @param first a string representation of a first name
     * @param last a string representation of a last name
     * @param telephone a string representation of a phone name
     */
    public Contact(String firstName, String lastName, String phone) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }
    /**
     * Returns a description of this contact as a string.
     *
     * @return a string representation of this contact
     */
    @Override
    public String toString() {
        return this.firstName+","+this.lastName+":"+this.phone;
    }
    /**
     * Uses both last and first names to determine lexical ordering.
     *
     * @param other the contact to be compared to this contact
     * @return the integer result of the comparison
     */
    @Override
    public int compareTo(Contact o) {
        int result;
        if(firstName.equals(o.firstName)){
            result = lastName.compareTo(o.lastName);
        }else{
            result = firstName.compareTo(o.firstName);
        }
        return result;
    }
}