package SecondTerm.week6;

public class gapsort {
    public static<T extends Comparable<? super T>> void sort(T[] data, int num){
        if(data.length == 0) {
            System.out.println("元素为空");
        }
        if(num <= data.length){
            int position, scan;
            T temp;
            //进行遍历轮数
            for(position = num; position >= 1; position = position - 2){
                //进行间隔为num的两个数的比较
                for(int length = data.length - 1; length > 0; length--){
                    scan = 0;
                    while(scan < data.length - position){
                        if(data[scan].compareTo(data[scan + position]) > 0){
                            temp = data[scan];
                            data[scan] = data[scan + position];
                            data[scan + position] = temp;
                        }
                        scan++;
                    }
                }
            }
            //间隔数减少至1
            num--;
        }else{
            System.out.println("间隔过长");
        }
    }
}